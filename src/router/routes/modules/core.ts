import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const core: AppRouteModule = {
  path: '/core',
  name: 'Core',
  component: LAYOUT,
  redirect: '/core/user',
  meta: {
    icon: 'eos-icons:system-group',
    title: '系统管理',
    orderNo: 1000,
  },
  children: [
    {
      path: 'user',
      name: 'UserPage',
      component: () => import('/@/views/core/user/index.vue'),
      meta: {
        title: '用户管理',
      },
    },
    {
      path: 'role',
      name: 'RolePage',
      component: () => import('/@/views/core/role/index.vue'),
      meta: {
        title: '角色管理',
      },
    },
    {
      path: 'permission',
      name: 'PermissionPage',
      component: () => import('/@/views/core/permission/index.vue'),
      meta: {
        title: '权限管理',
      },
    },
    {
      path: 'log',
      name: 'LogPage',
      component: () => import('/@/views/core/log/index.vue'),
      meta: {
        title: '日志管理',
      },
    },
  ],
};

export default core;
