import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const product: AppRouteModule = {
  path: '/product',
  name: 'Product',
  component: LAYOUT,
  redirect: '/product/site',
  meta: {
    icon: 'cib:app-store',
    title: '产品管理',
    orderNo: 10000,
  },
  children: [
    {
      path: 'site',
      name: 'SitePage',
      component: () => import('/@/views/product/site/index.vue'),
      meta: {
        title: '站点管理',
      },
    },
    {
      path: 'gateway',
      name: 'GatewayPage',
      component: () => import('/@/views/product/gateway/index.vue'),
      meta: {
        title: '网关管理',
      },
    },
    {
      path: 'device',
      name: 'DevicePage',
      component: () => import('/@/views/product/device/index.vue'),
      meta: {
        title: '设备管理',
      },
    },
    {
      path: 'msg',
      name: 'MsgPage',
      component: () => import('/@/views/product/msg/index.vue'),
      meta: {
        title: 'MQTT消息',
      },
    },
    {
      path: 'dev-info-msg',
      name: 'DevInfoMsgPage',
      component: () => import('/@/views/product/dev-info-msg/index.vue'),
      meta: {
        title: '设备数据消息',
      },
    },
    {
      path: 'dev-warn-msg',
      name: 'DevWarnMsgPage',
      component: () => import('/@/views/product/dev-warn-msg/index.vue'),
      meta: {
        title: '设备报警消息',
      },
    },
  ],
};

export default product;
