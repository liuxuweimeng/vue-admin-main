import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';

const account: AppRouteModule = {
  path: '/account',
  name: 'Account',
  component: LAYOUT,
  redirect: '/account/index',
  meta: {
    hideChildrenInMenu: true,
    icon: 'simple-icons:about-dot-me',
    title: '我的账户',
    orderNo: 100000,
  },
  children: [
    {
      path: 'index',
      name: 'AccountPage',
      component: () => import('/@/views/core/account/index.vue'),
      meta: {
        title: '我的账户',
        icon: 'simple-icons:about-dot-me',
        hideMenu: true,
      },
    },
  ],
};

export default account;
