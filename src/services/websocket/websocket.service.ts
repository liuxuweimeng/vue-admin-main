import {Es_Websocket} from '../../assets/js/eswebsocket';
import {Modal} from 'ant-design-vue';
import {createVNode} from 'vue';
import {ExclamationCircleOutlined} from '@ant-design/icons-vue';

/**
 * Websocket服务类
 *
 * TS引用JS的方式
 * https://www.cnblogs.com/zsg88/p/15621762.html
 * 修改tsconfig.json配置
 * "allowJs": true,        // 允许使用js
 * "noImplicitAny": false, // 不进行any语法检查
 * "include"需要引入js文件的位置，譬如"src/assets/js/*.js"
 *
 * 前端资源存放路径
 * /public/resource
 */
export class WebsocketService {

  // 最新安装包版本
  static MACOS_LATEST_VERSON = '1.0.0.2';

  // 安装包资源路径 /public/resource/plugin
  static MACOS_PLUGIN_PATH = '/resource/plugin/EsTSMCtrlSetup.pkg';

  constructor() {
  }

  static comparePackageVersion(version: string) {
    let isUpdate = false;
    if (WebsocketService.isWindows()) {
    }
    if (WebsocketService.isLinux()) {
    }
    if (WebsocketService.isMacOS()) {
      if (version < WebsocketService.MACOS_LATEST_VERSON) {
        isUpdate = true;
      }
    }
    if (isUpdate) {
      WebsocketService.confirmDownloadPackage('检测到有新版本安装包');
    }
  }

  static downloadPackage() {
    WebsocketService.confirmDownloadPackage('检测到可能未安装控件');
  }

  static confirmDownloadPackage(title: string) {
    Modal.confirm({
      title: title,
      icon: createVNode(ExclamationCircleOutlined),
      content: createVNode(
        'div',
        {style: 'color:red;'},
        "请点击'确认'下载控件安装包'EsTSMCtrlSetup'并手动安装，点击取消退出",
      ),
      okText: '确定',
      onOk() {
        let exportFileUrl: any = null;
        if (WebsocketService.isWindows()) {
        }
        if (WebsocketService.isLinux()) {
        }
        if (WebsocketService.isMacOS()) {
          exportFileUrl = WebsocketService.MACOS_PLUGIN_PATH;
        }
        if (exportFileUrl != null) {
          window.open(`${exportFileUrl}`);
        }
      },
    });
  }

  static getVersion() {
    return new Promise(function (resolve, reject) {
      const getVersion = Es_Websocket.getVersion as any;
      getVersion()
        .then((data) => {
          resolve(data);
        })
        .catch((reason) => {
          reject(reason);
        });
    });
  }

  static keyEnum() {
    return new Promise(function (resolve, reject) {
      const keyEnum = Es_Websocket.keyEnum as any;
      keyEnum()
        .then((data) => {
          resolve(data);
        })
        .catch((reason) => {
          reject(reason);
        });
    });
  }

  static sendApdu(seid, req) {
    return new Promise(function (resolve, reject) {
      const sendApdu = Es_Websocket.sendApdu as any;
      sendApdu(seid, req)
        .type((data) => {
          resolve(data);
        })
        .catch((reason) => {
          reject(reason);
        });
    });
  }

  static isWindows() {
    const isTrue = Es_Websocket.isWindows() as any;
    return isTrue;
  }

  static isMacOS() {
    const isTrue = Es_Websocket.isMacOS() as any;
    return isTrue;
  }

  static isLinux() {
    const isTrue = Es_Websocket.isLinux() as any;
    return isTrue;
  }

}
