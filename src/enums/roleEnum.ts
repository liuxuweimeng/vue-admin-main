export enum RoleEnum {
  // super admin
  SUPER = 'super',

  // tester
  TEST = 'test',

  // super manager
  SUPER_MNG = 'super-mng',

  // system manager
  SYSTEM_MNG = 'system-mng',
}
