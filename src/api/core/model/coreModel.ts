export interface QueryPageParam {
  query_type: number | string;
  query_content: string;
  page_index: number;
  page_size: number;
}

export interface HttpQueryPageResult {
  current_page: number;
  page_size: number;
  total_page: number;
  total_size: number;
  data: any;
}

export interface HttpQueryResult {
  code: number;
  data: HttpQueryPageResult;
  message: string;
}

export interface HttpResult {
  code: number;
  data: any;
  message: string;
}

export interface CreateUserParam {
  user_name: string;
  nick_name: string;
  intro: string;
}

export interface DeleteUserParam {
  id: number;
}

export interface ModifyUserParam {
  id: number;
  nick_name: string;
  gender: any;
  status: any;
  identity_number: string;
  address: string;
  intro: string;
}

export interface ModifyInfoParam {
  nick_name: string;
  gender: any;
  identity_number: string;
  address: string;
  intro: string;
}

export interface ModifyPwdParam {
  old_password: string;
  new_password: string;
}
