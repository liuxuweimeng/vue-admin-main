import {defHttp} from "/@/utils/http/axios";
import {
  CreateUserParam,
  DeleteUserParam,
  ModifyUserParam,
  HttpResult,
  QueryPageParam,
  HttpQueryResult,
} from "/@/api/core/model/coreModel";

enum UserApi {
  Create = '/api/user/create',
  Delete = '/api/user/delete',
  Modify = '/api/user/modify',
  QueryList = '/api/user/queryList',
}

export function createUser(params: CreateUserParam) {
  return defHttp.post<HttpResult>(
    {
      url: UserApi.Create,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function deleteUser(params: DeleteUserParam) {
  return defHttp.post<HttpResult>(
    {
      url: UserApi.Delete,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function modifyUser(params: ModifyUserParam) {
  return defHttp.post<HttpResult>(
    {
      url: UserApi.Modify,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function queryUserList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: UserApi.QueryList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}
