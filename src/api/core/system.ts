import {QueryPageParam, HttpQueryResult, HttpResult} from "/@/api/core/model/coreModel";
import {defHttp} from "/@/utils/http/axios";

enum SystemApi {
  QueryLogList = '/api/system/queryLogList',
  QueryAnalysisInfo = '/api/system/queryAnalysisInfo',
}

export function queryLogList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: SystemApi.QueryLogList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function queryAnalysisInfo() {
  return defHttp.post<HttpResult>(
    {
      url: SystemApi.QueryAnalysisInfo,
    },
    {
      errorMessageMode: 'none'
    });
}
