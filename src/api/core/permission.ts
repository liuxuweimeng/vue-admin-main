import {QueryPageParam, HttpQueryResult} from "/@/api/core/model/coreModel";
import {defHttp} from "/@/utils/http/axios";

enum PermissonApi {
  QueryList = '/api/permisson/queryList',
}

export function queryPermissonList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: PermissonApi.QueryList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}
