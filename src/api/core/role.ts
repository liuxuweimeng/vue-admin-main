import {QueryPageParam, HttpQueryResult} from "/@/api/core/model/coreModel";
import {defHttp} from "/@/utils/http/axios";

enum RoleApi {
  QueryList = '/api/role/queryList',
}

export function queryRoleList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: RoleApi.QueryList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}
