import {defHttp} from '/@/utils/http/axios';
import {LoginParams, LoginResultModel, GetUserInfoModel} from './model/userModel';
import {ErrorMessageMode} from '/#/axios';
import {HttpResult, ModifyInfoParam, ModifyPwdParam} from "/@/api/core/model/coreModel";

enum Api {
  Login = '/api/account/login',
  Logout = '/api/account/logout',
  GetUserInfo = '/api/account/getInfo',
  ModifyPwd = '/api/account/modifyPwd',
  ModifyInfo = '/api/account/modifyInfo',
  GetPermCode = '/api/account/getPermCode',
}

export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<LoginResultModel>(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

export function getUserInfo() {
  return defHttp.post<GetUserInfoModel>({url: Api.GetUserInfo}, {errorMessageMode: 'none'});
}

export function modifyUserPwd(params: ModifyPwdParam) {
  return defHttp.post<HttpResult>(
    {
      url: Api.ModifyPwd,
      params,
    },
    {
      errorMessageMode: 'none',
    },
  );
}

export function modifyUserInfo(params: ModifyInfoParam) {
  return defHttp.post<HttpResult>(
    {
      url: Api.ModifyInfo,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function getPermCode() {
  return defHttp.post<string[]>({url: Api.GetPermCode});
}

export function doLogout() {
  return defHttp.post({url: Api.Logout});
}
