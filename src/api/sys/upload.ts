import {defHttp} from '/@/utils/http/axios';
import {UploadFileParams} from '/#/axios';
import {useGlobSetting} from '/@/hooks/setting';
import {HttpResult} from "/@/api/core/model/coreModel";

const {uploadUrl = ''} = useGlobSetting();

/**
 * @description: Upload interface
 */
export function uploadApi(fileParams: UploadFileParams, otherParams: any) {
  return defHttp.uploadFileAndParam<HttpResult>(
    {
      url: uploadUrl,
    },
    fileParams,
    otherParams
  );
}
