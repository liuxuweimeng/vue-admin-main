/**
 * @description: Login interface parameters
 */
export interface LoginParams {
  account: string;
  password: string;
}

export interface RoleInfo {
  role_id: number;
  role_name: string;
  role_code: string;
}

/**
 * @description: Login interface return value
 */
export interface LoginResultModel {
  user_id: number;
  user_name: string;
  role_id: number;
  role_name: string;
  role_code: string;
  mail: string;
  mobile: string;
  token: string;
}

/**
 * @description: Get user information return value
 */
export interface GetUserInfoModel {
  // 用户id
  user_id: string | number;
  // 用户名
  user_name: string;
  // 真实名字
  nick_name: string;
  // 头像
  avatar: string;
  // 介绍
  intro?: string;
  // 邮箱
  mail: string;
  // 手机号
  mobile: string;
  // 角色
  roles: RoleInfo[];
}
