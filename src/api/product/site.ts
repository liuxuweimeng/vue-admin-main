import {defHttp} from "/@/utils/http/axios";
import {HttpQueryResult, HttpResult, QueryPageParam} from "/@/api/core/model/coreModel";
import {CreateSiteParam, DeleteSiteParam, ModifySiteParam} from "/@/api/product/model/productModel";

enum SiteApi {
  Create = '/api/site/create',
  Delete = '/api/site/delete',
  Modify = '/api/site/modify',
  QueryList = '/api/site/queryList',
}

export function createSite(params: CreateSiteParam) {
  return defHttp.post<HttpResult>(
    {
      url: SiteApi.Create,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function deleteSite(params: DeleteSiteParam) {
  return defHttp.post<HttpResult>(
    {
      url: SiteApi.Delete,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function modifySite(params: ModifySiteParam) {
  return defHttp.post<HttpResult>(
    {
      url: SiteApi.Modify,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function querySiteList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: SiteApi.QueryList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

