import {defHttp} from "/@/utils/http/axios";
import {HttpQueryResult, HttpResult, QueryPageParam} from "/@/api/core/model/coreModel";
import {
  CreateGatewayParam,
  DeleteGatewayParam,
  ModifyGatewayParam
} from "/@/api/product/model/productModel";
import {UploadFileParams} from "/#/axios";
import {useGlobSetting} from "/@/hooks/setting";

enum GatewayApi {
  Create = '/api/gateway/create',
  BatchCreate = '/api/gateway/batchCreate',
  Delete = '/api/gateway/delete',
  Modify = '/api/gateway/modify',
  QueryList = '/api/gateway/queryList',
}

export function createGateway(params: CreateGatewayParam) {
  return defHttp.post<HttpResult>(
    {
      url: GatewayApi.Create,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

const {apiUrl = ''} = useGlobSetting();
export function batchCreateGateway(fileParams: UploadFileParams) {
  return defHttp.uploadFileAndParam<HttpResult>(
    {
      url: apiUrl + GatewayApi.BatchCreate,
    },
    fileParams,
    []
  );
}

export function deleteGateway(params: DeleteGatewayParam) {
  return defHttp.post<HttpResult>(
    {
      url: GatewayApi.Delete,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function modifyGateway(params: ModifyGatewayParam) {
  return defHttp.post<HttpResult>(
    {
      url: GatewayApi.Modify,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function queryGatewayList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: GatewayApi.QueryList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}
