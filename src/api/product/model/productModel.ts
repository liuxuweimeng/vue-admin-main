
export interface CreateSiteParam {
  address: string;
  geo_lng: string;
  geo_lat: string;
  intro: string;
}

export interface DeleteSiteParam {
  id: string | number,
}

export interface ModifySiteParam {
  id: string | number,
  address: string;
  geo_lng: string;
  geo_lat: string;
  intro: string;
}

export interface CreateGatewayParam {
  site_id: string | number;
  mac_address: string;
  ccid: string;
  csq: string;
  imei: string;
  firmware_version: string;
  hardware_version: string;
  intro: string;
}

export interface DeleteGatewayParam {
  id: string | number,
}

export interface ModifyGatewayParam {
  id: string | number,
  site_id: string | number;
  mac_address: string;
  ccid: string;
  csq: string;
  imei: string;
  firmware_version: string;
  hardware_version: string;
  intro: string;
}

export interface DeviceParam{
  id: string | number,
}

export interface DeviceWarnParm{
  id: string | number,
  warn_code: string | number,
}

export interface ModifyDeviceParam{
  id: string | number,
  mac_address: string;
  intro: string;
}
