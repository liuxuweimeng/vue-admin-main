import {HttpQueryResult, QueryPageParam} from "/@/api/core/model/coreModel";
import {defHttp} from "/@/utils/http/axios";

enum MsgApi {
  QueryList = '/api/msg/queryList',
  QueryDevInfoList = '/api/msg/queryDevInfoList',
  QueryDevWarnList = '/api/msg/queryDevWarnList'
}

export function queryMsgList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: MsgApi.QueryList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function queryDevInfoMsgList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: MsgApi.QueryDevInfoList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function queryDevWarnMsgList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: MsgApi.QueryDevWarnList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}
