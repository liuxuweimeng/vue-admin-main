import {defHttp} from "/@/utils/http/axios";
import {HttpQueryResult, HttpResult, QueryPageParam} from "/@/api/core/model/coreModel";
import {DeviceParam, DeviceWarnParm, ModifyDeviceParam} from "/@/api/product/model/productModel";

enum DeviceApi {
  GetById = '/api/device/getById',
  Delete = '/api/device/delete',
  Modify = '/api/device/modify',
  QueryList = '/api/device/queryList',
  DeviceWarn = '/api/device/deviceWarn',
}

export function getDevById(params: DeviceParam) {
  return defHttp.post<HttpResult>(
    {
      url: DeviceApi.GetById,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function deleteDevice(params: DeviceParam) {
  return defHttp.post<HttpResult>(
    {
      url: DeviceApi.Delete,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function modifyDevice(params: ModifyDeviceParam) {
  return defHttp.post<HttpResult>(
    {
      url: DeviceApi.Modify,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function queryDeviceList(params: QueryPageParam) {
  return defHttp.post<HttpQueryResult>(
    {
      url: DeviceApi.QueryList,
      params
    },
    {
      errorMessageMode: 'none'
    });
}

export function deviceWarn(params: DeviceWarnParm) {
  return defHttp.post<HttpResult>(
    {
      url: DeviceApi.DeviceWarn,
      params
    },
    {
      errorMessageMode: 'none'
    });
}
