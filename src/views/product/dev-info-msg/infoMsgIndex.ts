export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
  },
  {
    title: '消息ID',
    dataIndex: 'msg_id',
    align: 'center',
  },
  {
    title: '消息类型',
    slots: {customRender: 'type'},
    align: 'center',
  },
  {
    title: '设备ID',
    dataIndex: 'device_id',
    align: 'center',
  },
  {
    title: '温度',
    slots: {customRender: 'temperature_value'},
    align: 'center',
  },
  {
    title: '电量',
    slots: {customRender: 'power'},
    align: 'center',
  },
  {
    title: '温度状态',
    slots: {customRender: 'temperature_state'},
    align: 'center',
  },
  {
    title: '防拆状态',
    slots: {customRender: 'tamper_state'},
    align: 'center',
  },
  {
    title: '温度校准状态',
    slots: {customRender: 'correct_state'},
    align: 'center',
  },
  {
    title: '操作',
    slots: {customRender: 'action'},
    align: 'center',
  },
];
