export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
  },
  {
    title: '数据ID',
    dataIndex: 'id',
    align: 'center',
  },
  {
    title: '消息ID',
    dataIndex: 'msg_id',
    align: 'center',
  },
  {
    title: '消息类型',
    slots: {customRender: 'type'},
    align: 'center',
  },
  {
    title: '主题',
    dataIndex: 'topic',
    align: 'center',
  },
  {
    title: '时间',
    dataIndex: 'create_time',
    align: 'center',
  },
  {
    title: '操作',
    slots: {customRender: 'action'},
    align: 'center',
  },
];
