export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
  },
  {
    title: '网关ID',
    dataIndex: 'gateway_id',
    align: 'center',
  },
  {
    title: '站点ID',
    dataIndex: 'site_id',
    align: 'center',
  },
  {
    title: 'MAC地址',
    dataIndex: 'mac_address',
    align: 'center',
  },
  {
    title: '状态',
    slots: {customRender: 'status'},
    align: 'center',
  },
  {
    title: '在线',
    slots: {customRender: 'online'},
    align: 'center',
  },
  {
    title: '操作',
    slots: {customRender: 'action'},
    align: 'center',
  },
];

