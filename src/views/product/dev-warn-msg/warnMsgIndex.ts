export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
  },
  {
    title: '消息ID',
    dataIndex: 'msg_id',
    align: 'center',
  },
  {
    title: '消息类型',
    slots: {customRender: 'type'},
    align: 'center',
  },
  {
    title: '设备ID',
    dataIndex: 'device_id',
    align: 'center',
  },
  {
    title: '报警码',
    slots: {customRender: 'warn_code'},
    align: 'center',
  },
  {
    title: '报警状态',
    slots: {customRender: 'warn_desc'},
    align: 'center',
  },
  {
    title: '操作',
    slots: {customRender: 'action'},
    align: 'center',
  },
];
