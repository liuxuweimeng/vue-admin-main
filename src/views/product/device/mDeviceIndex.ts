export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
  },
  {
    title: '设备ID',
    dataIndex: 'id',
    align: 'center',
  },
  {
    title: 'MAC地址',
    dataIndex: 'mac_address',
    align: 'center',
  },
  {
    title: '站点ID',
    dataIndex: 'site_id',
    align: 'center',
  },
  {
    title: '设备编号',
    dataIndex: 'device_no',
    align: 'center',
  },
  {
    title: '最新温度',
    slots: {customRender: 'temperature_value'},
    align: 'center',
  },
  {
    title: '最新电量',
    slots: {customRender: 'power'},
    align: 'center',
  },
  {
    title: '状态码',
    slots: {customRender: 'state'},
    align: 'center',
  },
  {
    title: '报警码',
    slots: {customRender: 'warn_code'},
    align: 'center',
  },
  {
    title: '操作',
    slots: {customRender: 'action'},
    align: 'center',
  },
];

