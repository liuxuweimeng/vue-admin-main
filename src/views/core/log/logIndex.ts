export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
  },
  {
    title: '模块',
    dataIndex: 'module',
    align: 'center',
  },
  {
    title: '操作',
    dataIndex: 'action',
    align: 'center',
  },
  {
    title: '操作人',
    slots: {customRender: 'name'},
    align: 'center',
  },
  {
    title: 'IP',
    dataIndex: 'ip',
    align: 'center',
  },
  {
    title: 'URL',
    dataIndex: 'url',
    align: 'center',
  },
  {
    title: '结果',
    dataIndex: 'result',
    align: 'center',
  },
  {
    title: '操作',
    slots: {customRender: 'action'},
    align: 'center',
  },
];

