export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
  },
  {
    title: '权限名称',
    dataIndex: 'name',
    align: 'center',
  },
  {
    title: '权限编码',
    dataIndex: 'code',
    align: 'center',
  },
  {
    title: '状态',
    slots: {customRender: 'status'},
    align: 'center',
  },
  {
    title: '操作',
    slots: {customRender: 'action'},
    align: 'center',
  },
];

