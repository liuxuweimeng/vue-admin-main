export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    align: 'center',
  },
  {
    title: '账号',
    dataIndex: 'user_name',
    align: 'center',
  },
  {
    title: '角色',
    dataIndex: 'role_name',
    align: 'center',
  },
  {
    title: '登录IP',
    dataIndex: 'last_login_ip',
    align: 'center',
  },
  {
    title: '状态',
    slots: {customRender: 'status'},
    align: 'center',
  },
  {
    title: '操作',
    slots: {customRender: 'action'},
    align: 'center',
  },
];

