# Core Vue Admin

## 我的仓库
https://gitee.com/liuxuweimeng/vue-admin-main.git

## 参考资料
https://vvbin.cn/doc-next/
<br>
https://github.com/anncwb/vue-vben-admin
<br>
https://v3.cn.vuejs.org/guide/introduction.html
<br>
https://www.vue3js.cn/docs/zh/guide/introduction.html
<br>
https://iconify.design
<br>
https://2x.antdv.com/docs/vue/introduce-cn
<br>
https://cn.vitejs.dev
<br>
https://blog.csdn.net/qq_39708228/article/list/2
    
## 使用注意点
```
1.src/enums/pageEnums.ts 调整BASE_HOME默认路由
2.src/utils/http/axios/userIndex.ts 将回响应数据进行设置 isTransformResponse: false
3.项目配置 src/settings/projectSetting.ts
4.多语言配置 src/settings/localeSetting.ts，可以开关语言选择器
5.关于因husky导致git commit失败，删除husky > pre-commit文件即可（https://blog.csdn.net/yehaocheng520/article/details/107284756）
6.关于生产模式编译，Antd需要在main.ts全量注入（目前不使用按需注入，https://vvbin.cn/doc-next/guide/component.html）
```
    
## 安装和使用
```
安装Node.js（推荐14.x及以上）
https://nodejs.org/en/
显示npm版本  npm -v
显示node版本 node -v

安装yarn 
npm i -g -yarn 
显示yarn版本  yarn -v

安装TypeScript
npm install -g typescript
```
```
请参考package.json下scripts项
支持npm及yarn编译

常用脚本
npm i或npm install 下载和安装项目依赖项
npm run dev/serve  开发环境编译
npm run bulid      生产环境编译/nginx环境配置
npm run preview    生产环境编译/本地环境配置

IDEA Debug调试
1.IDEA新增npm run serve配置
2.IDEA新增javascript debug配置
3.新增点击run运行npm run serve配置
4.再点击debug运行javascript debug配置
5.选中代码左侧边栏进行断点调试（前后端使用方式一致）
```
## 环境配置
```
.env 默认环境配置（其它环境均加载，同配置被其它环境覆盖）
.env.development 开发环境配置
.env.production  生产环境配置
.env.test        测试环境环境（一般不用）

环境变量的访问
import.meta.env.VITE_XXX （需要以VITE_做为开头）
```

## 技术分解

### Vue3生命周期
![Image text](https://mmbiz.qpic.cn/mmbiz_png/Bibnfj0bjiaRsicYVmbsiaaPVS8iaAWEiboUd98Tumunbia90HpNF8OThqlic0HNV0RGJJEkWzQUkv5uG99tI5zI8DX2tg/640?wx_fmt=png&wxfrom=5&wx_lazy=1&wx_co=1)
```
一般常用的是onMounted初始化函数和onUnmounted销毁函数
```
### Vue3语法
以下主要为相对vue2语法的差异
```
setup函数
1.setup函数替代vue2中的data和methid；setup的两个参数（prpos，{emit}），props一般接收父组件传过来的参数，emit则是注册回调事件
2.setup类似js中的闭包，需要将外部能访问的变量或方法通过return导出
```
```
ref函数
1.ref函数可以创建一个响应式数据，当数据变化时UI会更新
2.在template模板中ref的值不需要提供value获取，在ts或js中需要提供.value获取和赋值
3.可以做为某个模块的全局变量使用（vue3因setup函数的使用，vue2中在data中声明全局变量的方式废弃）
```
```
reactive函数
1.reactive函数也是替代vue2中data的一个函数
2.reactive函数可以声明data包装对象（类似json），在template模板中ref的值不需要提供data.xxx获取，在ts获取js中可以通过data.xxx获取和赋值
3.可以做为某个模块的全局变量使用
```
```
toRefs函数
1.使用toRefs函数对reactive函数包装的对象进行包装，确保使用扩展运算符进行解构之后，仍具有响应式
```
```
父组件—>子组件传参和回调
父组件 :xxx传参 @my-emit="callBack"传回调
子组件 使用Props函数接收父组件传过来的值, 使用Setup函数接收emit，使用 emit("my-emit", 'xxx')回调
```
```
import {computed, watch} from 'vue'
1.计算属性 computed
简单来说就是两个关联的变量，一个变量可以随着另外一个变量的变化而编号
const age = ref(18)
const nextAge = computed((){ return age.value+1})
修改age的值，nextAge的值也会跟着自动+1

2.监测属性 watch
watch(()=>age.value, (newValue, oldValue) => {
    // 大致和vue2用法一致
})
```

### 项目目录
```
.
├── build # 打包脚本相关
│   ├── config # 配置文件
│   ├── generate # 生成器
│   ├── script # 脚本
│   └── vite # vite配置
├── mock # mock文件夹
├── public # 公共静态资源目录
├── src # 主目录
│   ├── api # 接口文件
│   ├── assets # 资源文件
│   │   ├── icons # icon sprite 图标文件夹
│   │   ├── images # 项目存放图片的文件夹
│   │   └── svg # 项目存放svg图片的文件夹
│   ├── components # 公共组件
│   ├── design # 样式文件
│   ├── directives # 指令
│   ├── enums # 枚举/常量
│   ├── hooks # hook
│   │   ├── component # 组件相关hook
│   │   ├── core # 基础hook
│   │   ├── event # 事件相关hook
│   │   ├── setting # 配置相关hook
│   │   └── web # web相关hook
│   ├── layouts # 布局文件
│   │   ├── default # 默认布局
│   │   ├── iframe # iframe布局
│   │   └── page # 页面布局
│   ├── locales # 多语言
│   ├── logics # 逻辑
│   ├── main.ts # 主入口
│   ├── router # 路由配置
│   ├── settings # 项目配置
│   │   ├── componentSetting.ts # 组件配置
│   │   ├── designSetting.ts # 样式配置
│   │   ├── encryptionSetting.ts # 加密配置
│   │   ├── localeSetting.ts # 多语言配置
│   │   ├── projectSetting.ts # 项目配置
│   │   └── siteSetting.ts # 站点配置
│   ├── store # 数据仓库
│   ├── utils # 工具类
│   ├── services # 服务类模块
│   ├── model # model模块
│   └── views # 页面
├── test # 测试
│   └── server # 测试用到的服务
│       ├── api # 测试服务器
│       ├── upload # 测试上传服务器
│       └── websocket # 测试ws服务器
├── types # 类型文件
├── vite.config.ts # vite配置文件
└── windi.config.ts # windcss配置文件
```

### 项目配置
```
项目配置 src/settings/projectSetting.ts
缓存配置 src/settings/encryptionSetting.ts
多语言配置 src/settings/localeSetting.ts，可以开关语言选择器
全局主题色配置 build/config/glob/themeConfig.ts 
样式配置 src/settings/designSetting.ts 
组件配置 src/settings/componentSetting.ts （一般用不到，项目自定制为主）
```

### 项目路由
```
动态路由流程说明
1.用户登录接口，返回token
2.获取用户信息接口，需要返回的数据
userId: string | number;   
username: string;
realName: string;
avatar: string;
desc?: string;
// 默认路由页面-重要
homePath?: string;
// 用户角色信息
roles: RoleInfo[];
3.获取菜单路由信息 （同时获取按钮权限列表）
4.动态切换菜单路由
import { usePermission } from '/@/hooks/web/usePermission';
const { changeMenu } = usePermission();
// 先重新获取菜单
changeMenu();
```
```
Meta 配置说明（一般根据需要只用其中几个）
export interface RouteMeta {
  // 路由title  一般必填
  title: string;
  // 动态路由可打开Tab页数
  dynamicLevel?: number;
  // 动态路由的实际Path, 即去除路由的动态部分;
  realPath?: string;
  // 是否忽略权限，只在权限模式为Role的时候有效
  ignoreAuth?: boolean;
  // 可以访问的角色，只在权限模式为Role的时候有效
  roles?: RoleEnum[];
  // 是否忽略KeepAlive缓存
  ignoreKeepAlive?: boolean;
  // 是否固定标签
  affix?: boolean;
  // 图标，也是菜单图标
  icon?: string;
  // 内嵌iframe的地址
  frameSrc?: string;
  // 指定该路由切换的动画名
  transitionName?: string;
  // 隐藏该路由在面包屑上面的显示
  hideBreadcrumb?: boolean;
  // 如果该路由会携带参数，且需要在tab页上面显示。则需要设置为true
  carryParam?: boolean;
  // 隐藏所有子菜单
  hideChildrenInMenu?: boolean;
  // 当前激活的菜单。用于配置详情页时左侧激活的菜单路径
  currentActiveMenu?: string;
  // 当前路由不再标签页显示
  hideTab?: boolean;
  // 当前路由不再菜单显示
  hideMenu?: boolean;
  // 菜单排序，只对第一级有效
  orderNo?: number;
  // 忽略路由。用于在ROUTE_MAPPING以及BACK权限模式下，生成对应的菜单而忽略路由。2.5.3以上版本有效
  ignoreRoute?: boolean;
  // 是否在子级菜单的完整path中忽略本级path。2.5.3以上版本有效
  hidePathForChildren?: boolean;
}
```
```
前端默认首路由（需要和后端返回的一致，通用默认路由）
src/enums/pageEnums.ts
export enum PageEnum {
    // 更改此处即可
    BASE_HOME = '/welcome',
}
```
```
路由刷新（重定向方式）
import { useRedo } from '/@/hooks/web/usePage';
const redo = useRedo();
// 执行刷新
redo();
```
```
页面跳转
import { useGo } from '/@/hooks/web/usePage';
const go = useGo();
// 执行刷新
go();
go(PageEnum.Home);
```

### 权限控制
```
前端权限控制的三种方式
通过用户角色来过滤菜单(前端方式控制)，菜单和路由分开配置（静态路由，譬如数币项目就是这种方式）
通过用户角色来过滤菜单(前端方式控制)，菜单由路由配置自动生成
通过后台来动态生成路由表(后台方式控制，详情请看本篇'项目路由'，新版TSM项目主用这种方式)

细粒度权限也称按钮权限
项目中可以使用以下方式控制（在获取菜单路由时同时从后台获取按钮权限列表，一般返回的是权限编码）
1.hasPermission src/hooks/web/usePermission.ts实现原理
<a-button v-if="hasPermission(['20000', '2000010'])" color="error" class="mx-4">
    拥有[20000,2000010]code可见
</a-button>
2.v-auth src/directives/permission.ts实现原理
<a-button v-auth="'1000'" type="primary" class="mx-4"> 拥有code ['1000']权限可见 </a-button>
```

### Mock服务
```
Mock是方便本地联调的代理模拟服务器
Mock接口格式
{
  url: string; // mock 接口地址
  method?: MethodType; // 请求方式
  timeout?: number; // 延时时间
  statusCode: number; // 响应状态码
  response: ((opt: { // 响应结果
      body: any;
      query: any;
  }) => any) | object;
}

# 配置Mock开启和关闭（生产模式默认关闭）
VITE_USE_MOCK = true;
# vite 跨域代理，检测到/basic-api的情况下会访问跨域IP
VITE_PROXY=[["/basic-api","http://192.168.1.1:3000"]]
# 默认接口地址，MOCK开启后，默认访问本地IP
VITE_GLOB_API_URL=/api

vite跨域配置项（该项目中配置了VITE_PROXY会自动处理，不需要手动配置）
server: {
    proxy: {
      "/basic-api":{
      target: 'http://192.168.1.1:3000',
      changeOrigin: true,
      ws: true,
      rewrite: (path) => path.replace(new RegExp(`^/basic-api`), ''),
    }
}
```

### Axios HTTP
```
axios配置
axios请求 src/utils/http/axios
目前导出的defHttp为默认实例

axios响应配置
src/utils/http/axios/userIndex.ts
// 需要对返回数据进行处理，需要配置为false
isTransformResponse: false,

多接口地址配置
// 需要有其他接口地址的可以在后面添加
// other api url
export const otherHttp = createAxios({
  requestOptions: {
    apiUrl: 'xxx',
  },
});

```

### Vite开发和构建
```
https://cn.vitejs.dev/guide/#community-templates
优势：速度快，功能全，淘汰了webpack，vue3主流

安装vite
npm init vite@latest
yarn create vite
构建vite
npm init vite@latest my-vue-app -- --template vue
yarn create vite my-vue-app --template vue
```
```
多入口模式
https://cn.vitejs.dev/guide/build.html#multi-page-app
使用场景：游客模式，后台管理模式

// __dirname 的值代表 vite.config.js 文件所在的目录
const { resolve } = require('path')
const { defineConfig } = require('vite')
module.exports = defineConfig({
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'index.html'),
        nested: resolve(__dirname, 'nested/index.html')
      }
    }
  }
})

// index.html需要引入各自的main.ts
// 可以通过a标签进行入口切换
<div><a href="/index.html">后台</a></div>

```
